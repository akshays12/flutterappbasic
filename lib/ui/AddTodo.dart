import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:todo_app/model/TodoModel.dart';

class AddTodo extends StatelessWidget {
  final String userName;
  final TextEditingController taskName = TextEditingController();
  AddTodo({this.userName});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("TodoApp"),
        backgroundColor: Colors.lightBlueAccent,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            Expanded(
                child: TextField(
              decoration: InputDecoration(
                labelText: 'ADD TODO TASK',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
              ),
              controller: taskName,
            )),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  RaisedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text('Cancel'),
                  ),
                  RaisedButton(
                    onPressed: () async {
                      Dio dio = Dio();
                      Response response = await dio.post(
                        "http://10.0.2.2:8080/todo/saveTaskAndCompleted",
                        options: Options(
                          contentType: ContentType.json.toString(),
                        ),
                        data: TODO(
                          taskName: taskName.text,
                          isCompleted: false,
                          name: userName,
                        ).toJson(),
                      );
                      print(response.statusCode);

                      await Fluttertoast.showToast(
                          msg: "${taskName.text} Task Added",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 16.0);
                      Navigator.of(context).pop();
                    },
                    child: Text('Submit'),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
