import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todo_app/model/TodoModel.dart';
import 'package:todo_app/ui/AddTodo.dart';
import 'package:http/http.dart' as http;

class HomeScreen extends StatefulWidget {
  final String userName;
  HomeScreen({@required this.userName});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool isCompleted;

  @override
  void initState() {
    isCompleted = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("TodoApp"),
        backgroundColor: Colors.lightBlueAccent,
      ),
      body: Container(
        child: Column(
          children: [
            DropdownButton(
              value: isCompleted,
              items: [
                DropdownMenuItem(
                  child: Text('Completed'),
                  value: true,
                ),
                DropdownMenuItem(
                  child: Text('Incompleted'),
                  value: false,
                ),
              ],
              onChanged: (val) {
                setState(() {
                  isCompleted = val;
                });
              },
            ),
            Expanded( 
              flex: 5,
              child: StreamBuilder<http.Response>(
                  stream: http
                      .get(
                          "http://10.0.2.2:8080/todo/getAllTaskFromUsername?name=${widget.userName}")
                      .asStream(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    Map<String, TODO> todosMap = Map<String, TODO>();
                    if (snapshot.data.statusCode == 200) {
                      Map<String, dynamic> todos =
                          json.decode(snapshot.data.body);
                      todos['todos']?.forEach((k, v) {
                        todosMap[k] = TODO.fromJson(json.decode(v));
                      });
                    }

                    Map<String, TODO> todos;
                    todos = Map<String, TODO>();
                    todosMap.forEach((k, todo) {
                      if (todo.isCompleted == isCompleted) {
                        todos[k] = (todo);
                      }
                    });
                    
                    return ListView.builder(
                      itemCount: todos.length,
                      itemBuilder: (context, ind) {
                        String index = todos.keys.toList()[ind];
                        return Card(
                          child: ListTile(
                            title: Text(todos[index].taskName),
                            trailing: CircleAvatar(
                              backgroundColor: isCompleted
                                  ? Color(0xFF5BFF29)
                                  : Color(0xFF999999),
                              radius: 20,
                              child: IconButton(
                                icon: Icon(
                                  Icons.done,
                                ),
                                onPressed: () async {
                                  todos[index].isCompleted = !isCompleted;
                                  Dio dio = Dio();
                                  Response response = await dio.post(
                                      "http://10.0.2.2:8080/todo/updateIsCompletedForGivenTask",
                                      options: Options(
                                        contentType:
                                            ContentType.json.toString(),
                                      ),
                                      data: todos[index].toJson());
                                  setState(() {});
                                },
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }),
            ),
            Expanded(
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 24.0),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Back'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => AddTodo(
                userName: widget.userName,
              ),
            ),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
