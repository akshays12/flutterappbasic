class TODO {
  String sId;
  String sRev;
  String name;
  String taskName;
  bool isCompleted;

  TODO({this.sId, this.sRev, this.name, this.taskName, this.isCompleted});

  TODO.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    sRev = json['_rev'];
    name = json['name'];
    taskName = json['taskName'];
    isCompleted = json['isCompleted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['_rev'] = this.sRev;
    data['name'] = this.name;
    data['taskName'] = this.taskName;
    data['isCompleted'] = this.isCompleted;
    return data;
  }
}
