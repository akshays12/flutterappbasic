import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:todo_app/model/TodoModel.dart';
import 'package:todo_app/ui/HomeScreen.dart';
import 'package:http/http.dart' as http;

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'TODO APP',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new HelloUser(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HelloUser extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HelloUserState();
}

class _HelloUserState extends State<HelloUser> {
  String name = '';
  Widget submitButtonWidget;
  @override
  void initState() {
    submitButtonWidget = Text('Submit');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("TodoApp"),
        backgroundColor: Colors.lightBlueAccent,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Column(
                children: [
                  TextField(
                    decoration: InputDecoration(
                      labelText: 'USERNAME',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                    onChanged: (String string) {
                      setState(() {
                        name = string;
                      });
                    },
                  ),
                  Text('Hello ' + name + '!'),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 8.0, bottom: 16.0),
              child: Align(
                alignment: Alignment.bottomRight,
                child: RaisedButton(
                  child: submitButtonWidget,
                  onPressed: () async {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => HomeScreen(
                          userName: name,
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/*class SubmitButton extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    var button =Container(
      child: Text("Submit Username"),
      color: Colors.blue,
      elevation: 5.0,
      onPressed: () {
        submit(context);
      }

    )
    throw UnimplementedError();
  }

}*/
